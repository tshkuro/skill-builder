import React, { Component } from 'react';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import './App.css';
import reducer from './reducers/counter';
import rootSaga from './sagas/sagas';
import Counter from './Counter';

const sagaMiddlware = createSagaMiddleware();
const store = createStore(
  reducer,
  applyMiddleware(sagaMiddlware)
);
sagaMiddlware.run(rootSaga);

const action = type => store.dispatch({ type });

class App extends Component {
  render() {
    // console.log(store.getState());
    console.log(this.props);
    return (
      <Provider store={store}>
      <div>
        <div>
          <h2>Welcome to React</h2>
        </div>
        <Counter
          // value={store.getState()}
          onIncrement={() => action('INCREMENT')}
          onDecrement={() => action('DECREMENT')}
          onIncrementAsync={() => action('INCREMENT_ASYNC')}
        />
      </div>
      </Provider>

    );
  }
}

export default App;