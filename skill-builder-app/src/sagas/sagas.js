import { put, takeEvery, all, call } from 'redux-saga/effects';

function* helloSaga() {
    console.log('Hello sagas');
}

const delay = ms => new Promise(res => setTimeout(res, ms));
export function* incrementAsyc() {
    yield call(delay, 1000);
    yield put({ type: 'INCREMENT' });
}

function* watchIncrementAsync() {
    yield takeEvery('INCREMENT_ASYNC', incrementAsyc);
}

export default function* rootSaga() {
    yield all([
        helloSaga(),
        watchIncrementAsync()
    ]);
}