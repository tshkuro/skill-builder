import React from 'react';
import { connect } from 'react-redux';

const Counter = ({value, onIncrement, onDecrement, onIncrementAsync}) => (
    <div className="counter">
        <button onClick={onIncrement}>Increment</button>
        <button onClick={onDecrement}>Decrement</button>
        <button onClick={onIncrementAsync}>Increment async</button>
        
        <div>
            Clicked: {value} times
        </div>
    </div>
);

const mapStateToProps = state => ({
    value: state
});

export default connect(mapStateToProps)(Counter);